import React, { useState, useEffect } from 'react';
import moment from 'moment';
import './Table.scss';
import { ChevronLeft, ChevronRight, Visibility, Create, Delete } from "@material-ui/icons";

const Table = (props) => {
    const columns = props.columns;
    const rows = props.rows;
    const [pageStart, setPageStart] = useState(0);
    const [pageEnd, setPageEnd] = useState(props.limit ? props.limit : rows.length);
    const [pageNumber, setPageNumber] = useState(1);
    const [totalPages, setTotalPages] = useState(0);

    const sliceString = (string, count) => {
        if (count) {
            return string.slice(0, count) + "...";
        } else {
            return string;
        }
    }

    const paginateRows = (rows) => {
        return rows.slice(pageStart, pageEnd);
    }

    useEffect(() => {
        let totalPages = 0;
        if (rows) {
            totalPages = parseInt(Math.ceil((rows.length / props.limit)).toFixed(0));
        }
        setTotalPages(totalPages);
    }, [props.limit, rows]);

    const paginate = (val) => {
        if (val === 1) {
            if (pageNumber < totalPages) {
                setPageStart(pageStart + props.limit);
                setPageEnd(pageEnd + props.limit);
                setPageNumber(pageNumber + 1);
            }
        } else {
            if (pageNumber > 1) {
                setPageStart(pageStart - props.limit);
                setPageEnd(pageEnd - props.limit);
                setPageNumber(pageNumber - 1);
            }
        }
    }

    const hasActions = () => {
        return props.viewAction ||
            props.editAction ||
            props.deleteAction;
    }


    const renderTableRows = (column,row) => {
        switch(column.field) {
            case 'date':
                return moment(new Date(row[column.field])).format('MMM-DD-YYYY');
            case 'time_spent':
                return <p className='clock'>{row[column.field].toString().slice(0,8)}</p>
            default:
                return sliceString(row[column.field], column.sliceCount);
        }
    }

    return (
        <div className="table">
            <div className="table-content">
                <div className="table-header">
                    {
                        columns.map((header, key) => (
                            <div
                                className={`table-header-items flex-${header.flexVal} text-${header.align}`}
                                key={key}
                            >
                                {header.label}
                            </div>
                        ))
                    }
                    {
                        hasActions() &&
                            <div className={`action-title`}>Actions</div>
                    }
                </div>
                {
                    rows && rows.length > 0 ?
                        <div className="table-body">
                            {
                                paginateRows(rows).map((rowData, key) => (

                                    <div className="table-row" key={key}>
                                        {
                                            columns.map((columnHeader, key) =>
                                                (
                                                    <div
                                                        className={`table-body-items flex-${columnHeader.flexVal} text-${columnHeader.align}`}
                                                        key={key}
                                                    >
                                                        <span>
                                                            {
                                                                renderTableRows(columnHeader,rowData)
                                                            }
                                                        </span>
                                                    </div>
                                                ))
                                        }
                                        {
                                            hasActions() &&
                                                (
                                                    <div className="table-actions d-flex justify-around">
                                                        {props.viewAction ? <div><Visibility className="table-action-item" onClick={() => props.viewAction(rowData)} /></div>: ''}
                                                        {props.editAction ? <div><Create className="table-action-item" onClick={() => props.editAction(rowData)} /></div>: ''}
                                                        {props.deleteAction ? <div><Delete className="table-action-item" onClick={() => props.deleteAction(rowData)} /></div>: ''}
                                                    </div>
                                                )
                                        }
                                    </div>
                                ))
                            }
                        </div> :
                        <div className="flex-centered">No Items Found.</div>
                }
            </div>
            {
                props.limit ?
                    <div className="pagination-area">
                        <label>Page {pageNumber} of {totalPages}</label>
                        <div className="button-area flex items-center">
                            <i className="material-icons pointer" onClick={() => paginate(-1)}><ChevronLeft /></i>
                            <i className="material-icons pointer" onClick={() => paginate(1)}><ChevronRight /></i>
                        </div>
                    </div> :
                    ''
            }
        </div>
    )
}

export default Table;
