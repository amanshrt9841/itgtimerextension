import * as actionTypes from "./ToasterTypes";

const initialState = {
  appear: false,
  title: "",
  name: "",
  message: "",
};

const utilReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_TOASTER:
      return {
        ...state,
        appear: action.appear,
        title: action.title,
        name: action.name,
        message: action.message,
      };
    default:
      return state;
  }
};

export default utilReducer;
