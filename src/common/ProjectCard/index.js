import React from "react";
import './projectCard.scss'
import { MoreVert } from "@material-ui/icons";

const ProjectCard = (props) => {
    const { projectName, projectDuration, durationUnit, projectTaskName } = props;

    return(
        <>
            <div className='project-card'>
                <div className='project-title d-flex justify-between'>
                    <p className='sub-title bold'>{projectName}</p>
                   <div className='pointer'> <MoreVert/></div>
                </div>
                <div className='project-content d-flex flex-column'>
                    {
                        projectTaskName &&
                        <div className='task-name'>
                            <div className='bold'>Task:</div>
                            <p>{projectTaskName}</p>
                        </div>
                    }
                    <div className='time-spent'>
                        <div className='bold'>Total Time spent</div>
                        <p className='sub-title bold'>{projectDuration} {durationUnit}</p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProjectCard;