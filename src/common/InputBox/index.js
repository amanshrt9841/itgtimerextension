import React from 'react';

const InputBox = (props) => {
    return (
        <div className="flex-1">
            <div className="input-group">
                <label>
                    {props.label}{props.required === true ? <span>*</span> : ''}
                </label>
                <div className={`input-box ${props.styles}`}>
                    {
                        props.inputType === "textarea" ?
                            <textarea
                                rows={props.rows}
                                name={props.name}
                                value={props.value}
                                placeholder={props.placeholder}
                                onChange={(e) => props.onChange(e)}
                                required={props.required}
                            ></textarea> :
                            <input
                                type={props.type}
                                name={props.name}
                                value={props.value}
                                placeholder={props.placeholder}
                                onChange={(e) => props.onChange(e)}
                                required={props.required}
                            />
                    }
                </div>
            </div>
        </div>
    )
}

export default InputBox;
