import React from "react";
import './taskCard.scss';
import moment from 'moment';
import { CalendarToday } from "@material-ui/icons";

const TaskCard = (props) => {
    const { username, taskDate, taskName, description } = props;

    let shortDate = moment(taskDate).format('MMM DD');

    return(
        <>
            <div className='task-card'>
                <div className='task-heading d-flex justify-between align-center'>
                    <div className="user">{username}</div>
                    <div className="d-flex align-center"><CalendarToday />{shortDate}</div>
                </div>
                <div className='task-content d-flex flex-column'>
                    <div className='task-name'>
                        <div className='bold'>Task:</div>
                        <p>{taskName}</p>
                    </div>
                    <div className='description'>
                        <div className='bold'>Description</div>
                        <p>{description}</p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default TaskCard;