import React from "react";
import "./listTable.scss";

const ListTable = (props) => {
    const columns = props.columns;
    const rows = props.rows.slice(0, props.limit);

    const sliceString = (string, count) => {
        if (count) {
            return string.slice(0, count) + "...";
        } else {
            return string;
        }
    }

    return (
        <div className="list-table">
            <div className="table-header">
                {
                    columns.map((header, key) => (
                        <div
                            className={`table-header-items flex-${header.flexVal} text-${header.align}`}
                            key={key}
                        >
                            {header.label}
                        </div>
                    ))
                }
            </div>
            {
                rows && rows.length > 0 ?
                    <div className="table-body">
                        {
                            rows.map((rowData, key) => (
                                
                                <div className="table-row" key={key}>
                                    {
                                        columns.map((columnHeader, key) =>
                                            (
                                                <div
                                                    className={`table-body-items flex-${columnHeader.flexVal} text-${columnHeader.align}`}
                                                    key={key}
                                                >

                                                    <span className={`${columnHeader.boxed ? rowData[columnHeader.field] : ''}`}>
                                                        {
                                                            sliceString(rowData[columnHeader.field], columnHeader.sliceCount)
                                                        }
                                                    </span>
                                                </div>
                                            ))
                                    }
                                </div>
                            ))
                        }
                    </div> :
                    <div className="flex-centered">No Items Found.</div>
            }
        </div>
    )
}

export default ListTable;