import * as actionTypes from './authenticationType'

const initialState = {
    isLoggedIn: false,
    loginUser:{},
    userType:''
}

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.LOGIN_USER:
            return {
                ...state,
                loginUser: actions.user,
                userType: actions.user.type,
                isLoggedIn:true,
            };
        case actionTypes.LOGOUT_USER:
            return {
                ...state,
                loginUser: {},
                isLoggedIn:false
            }
        default:
            return state
    }
}
