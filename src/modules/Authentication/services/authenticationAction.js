import * as actionType from './authenticationType'
import apolloClient from "../../../config/apolloclient";
import * as actions from '../../../store/actions'

import gql from "graphql-tag";
import {UserType} from "../../../enum/UserType";

const authSuccess = (user) => {
    return {
        type: actionType.LOGIN_USER,
        user: user
    }
}

const authFail = () => {
    return {
        type: actionType.LOGOUT_USER
    }
}

const searchUser = (email)  => {
    return apolloClient.mutate(
        {
            mutation: gql`
                query MyQuery($email : String!) {
                    users(where: {email: {_eq: $email}}) {
                        email
                        id
                        username
                        password
                        type
                    }
                }
            `,
            variables: {
                email : email
            }
        }
    )
}

const checkPassword = (passwordEnter, originalPasword) => {
    if(originalPasword === passwordEnter){
        return true
    }else{
        return false
    }
}

export const checkAuthentication = () => (dispatch) => {
    try {
        dispatch(actions.setLoading('login'))
        let authUser = localStorage.getItem("user");
        if (authUser) {
            const userData = JSON.parse(authUser);
            dispatch(actions.clearLoading())
            dispatch(authSuccess(userData));
        } else {
            dispatch(actions.clearLoading())
            dispatch(authFail());
        }
    }catch (e) {

    }

}


export const loginAdmin = (user) =>  async dispatch => {
    try{
        dispatch(actions.setLoading('login'))
        const existedUser = (await searchUser(user.email)).data.users[0]
        const isAdmin = existedUser.type === UserType.admin;
        const isMatch = await checkPassword(user.password,existedUser.password)
        if (isMatch && isAdmin) {
          localStorage.setItem("user", JSON.stringify(existedUser));
          dispatch(
            actions.setToasterState(
              true,
              "Success",
              "Login Success",
              `Welcome ${user.email}`
            )
          );
          dispatch(authSuccess(existedUser));
          dispatch(actions.clearLoading());
        } else {
          dispatch(
            actions.setToasterState(
              true,
              "Error",
              "Authentication Problem",
              "Login Failed"
            )
          );
          dispatch(actions.clearLoading());
          dispatch(authFail());
        }
    }catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Authentication Problem",
                "Login Failed"
            )
        )
        dispatch(actions.clearLoading())
        dispatch(authFail());
    }

}

export const loginUser = (user) =>  async dispatch => {
    try{
        dispatch(actions.setLoading('login'))
        const existedUser = (await searchUser(user.email)).data.users[0]
        const isUser = existedUser.type === UserType.user;
        const isMatch = await checkPassword(user.password,existedUser.password)
        if (isMatch && isUser) {
          localStorage.setItem("user", JSON.stringify(existedUser));
            localStorage.setItem("loginTime", Date.now());
          dispatch(
            actions.setToasterState(
              true,
              "Success",
              "Login Success",
              `Welcome ${user.email}`
            )
          );
          dispatch(authSuccess(existedUser));
          dispatch(actions.clearLoading());
        } else {
          dispatch(
            actions.setToasterState(
              true,
              "Error",
              "Authentication Problem",
              "Login Failed"
            )
          );
          dispatch(actions.clearLoading());
          dispatch(authFail());
        }
    }catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Authentication Problem",
                "Login Failed"
            )
        )
        dispatch(actions.clearLoading())
        dispatch(authFail());
    }
}

export const logoutUser = () => dispatch => {
    try{
        dispatch(actions.setLoading())
        localStorage.removeItem("user");
        dispatch(authFail());
        dispatch(
          actions.setToasterState(
            true,
            "Success",
            "Authentication",
            "Logout Success"
          )
        );
    }catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Authentication Fail",
                "Logout Failed"
            )
        )
        dispatch(actions.clearLoading())
    }
}

export const registerUser = (user) => dispatch => {
    //sign up user logic goes here  with dispatch of two action authSuccess or authFail
}
