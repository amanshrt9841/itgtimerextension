import React from 'react';
import Loading from "../../../common/Loading";

const Signup = () => {
    return (
        <section className="signup">
            <form action="" className={"signup-form"}>
                <div className="form-group">
                    <div className="input-group">
                        <div className="input-box">
                            <input type="email" name={"email"} placeholder={"Your email goes here"}/>
                        </div>

                    </div>
                </div>

                <div className="form-group">
                    <div className="input-group">
                        <div className="input-box">
                            <input type="password" name={"password"} placeholder={"Your password here"}/>
                        </div>
                    </div>
                </div>

                <div className="form-group">
                    <div className="btn primary d-flex justify-evenly align-center">
                        <div className="text">
                            Login
                        </div>
                        <Loading isButton={true}/>
                    </div>
                </div>
            </form>
        </section>
    );
};

export default Signup;
