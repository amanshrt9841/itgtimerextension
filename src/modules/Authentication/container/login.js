import React, {useState} from 'react';
import '../authentication.scss'
import Loading from "../../../common/Loading";
import {useDispatch, useSelector} from "react-redux";
import * as actions from '../services/authenticationAction'
import loginImage from "../../../assets/images/login-image.svg";
import {Lock, Person} from "@material-ui/icons"
import AuthRules from "../AuthRules";
import {ValidateForm, ValidateInput} from "../../../utills/validation";

const initialValue = {
    email: '',
    password: ''
}

const Login = () => {
    const [user, setUsers] = useState(initialValue)
    const [errors, setErrors] = useState(initialValue);
    const dispatch = useDispatch()
    const {loadingState} = useSelector(state => state.loadingReducer)
    const handleChange = (e) => {
        e.persist()
        setUsers({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    const inputValidation = (e) => {
        let errorMsg = ValidateInput(e.target.name, e.target.value, AuthRules);
        setErrors({
            ...errors,
            [e.target.name]: errorMsg
        });
    }

    const handleSubmit = async () => {
        let errorMsg = ValidateForm(user, AuthRules);
        setErrors({...errorMsg});
        let validated = Object.values(errorMsg).join('').length === 0;
        if(validated){
            (window.location.href).includes('admin') ? dispatch(actions.loginAdmin(user)) : dispatch(actions.loginUser(user))
        }
    }

    return (
        <>
            <section className="login">
                <div className='left-section'>
                    <img src={loginImage} alt=""/>
                    <div className='login-text white-text text-center'>
                        <p className="login-1st-line">A good plan today</p>
                        <p className="login-2nd-line">is better than</p>
                        <p className='login-3rd-line'>perfect plan tomorrow</p>
                    </div>
                </div>
                <div className={"login-form"}>
                    {
                        (window.location.href).includes('admin')
                            ? (
                                <div className="title text-center">
                                    Login as Admin
                                </div>
                            ) : (
                                <div className='login-title flex flex-column'>
                                    <p className='primary-text title'>Project Planner</p>
                                    <p className='sub-title'>Track your <span className='bold'>Workflow</span></p>
                                    <div className='dash'></div>
                                    <p className='sub-title bold uppercase'>Login</p>
                                </div>
                            )
                    }
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-box">
                                <Person/>
                                <input
                                    type="email"
                                    name={"email"}
                                    placeholder={"Email"}
                                    onChange={(e) => {
                                        handleChange(e);
                                        inputValidation(e);
                                    }}
                                />
                            </div>
                        </div>
                        {errors.email !== '' ? <span className="error-text">{errors.email}</span> : ''}
                    </div>
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-box">
                                <Lock/>
                                <input
                                    type="password"
                                    name={"password"}
                                    placeholder={"Password"}
                                    onChange={(e) => {
                                        handleChange(e);
                                        inputValidation(e);
                                    }}
                                />
                            </div>
                        </div>
                        {errors.password !== '' ? <span className="error-text">{errors.password}</span> : ''}
                    </div>
                    <div className="form-group">
                        <div
                            className="btn primary uppercase bold d-flex justify-evenly align-center"
                            onClick={() => handleSubmit()}
                        >
                            <div>Login</div>
                            {loadingState === "login" && <Loading isButton={true}/>}
                        </div>
                    </div>
                    <div className='forgot-password text-center bold pointer'>
                        <p>Forgot Password?</p>
                    </div>
                </div>
            </section>
        </>
    );
};

export default Login;


