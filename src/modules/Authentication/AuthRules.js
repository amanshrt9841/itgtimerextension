export default {
    email: {
        required: true,
        isEmail: true,
        label: 'Email'
    },
    password: {
        required: true,
        minValue: 3,
        label: 'Password'
    },
    confirmPassword: {
        required: true,
        minValue: 8,
        label: 'Confirm Password'
    }
}