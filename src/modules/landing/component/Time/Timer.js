import React, { Component } from "react";

export default class Timer extends Component {
  state = {
    hours: 0,
    minutes: 0,
    seconds: 0,
    pause: false,
  };

  componentDidMount() {
    this.myInterval = setInterval(() => {
      const { hours, seconds, minutes, pause } = this.state;

      console.log(this.state.pause);
      if (pause === false) {
        this.setState(({ seconds }) => ({
          seconds: seconds + 1,
        }));
      }

      if (seconds === 60) {
        this.setState(({ minutes }) => ({
          hours: hours,
          minutes: minutes + 1,
          seconds: 0,
        }));
      }

      if (minutes === 60) {
        this.setState(({ minutes }) => ({
          hours: hours + 1,
          minutes: 0,
          seconds: 0,
        }));
      }
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.myInterval);
  }

  handlePause = () => {
    this.setState({
      pause: !this.state.pause,
    });
  };

  handleStop() {
    this.setState({
      pause: !this.state.pause,
    });
  }

  render() {
    const { hours, minutes, seconds } = this.state;
    return (
      <>
        <div>
          <h1>
            Time: {hours}:{minutes < 10 ? `0${minutes}` : minutes}:
            {seconds < 10 ? `0${seconds}` : seconds}
          </h1>
        </div>
        <div>
          <button onClick={this.handlePause}>Pause</button>
          <button onClick={this.handleStop}>Stop</button>
        </div>
      </>
    );
  }
}
