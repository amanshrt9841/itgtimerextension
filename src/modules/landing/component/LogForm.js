import React, { useState, useEffect } from "react";
import { useDispatch } from 'react-redux';
import './logform.scss';
import * as actions from './services/logAction';
import Toaster from "../../../common/Toaster/Toaster"
import * as action from "../../../store/actions";
import { Redirect } from "react-router-dom";
const LogForm = () => {
  const [logData, setLogData] = useState({
    title: '',
    details: '',
    duration: '',
  });
  const [timeDisplay, setTimeDisplay] = useState("");
  const [logScreenStatus, setLogscreenStatus] = useState(false);
  const [timerStopped, setTimeStopped] = useState(localStorage.getItem("timerStopped") || false);
  const { title, details, duration } = logData;

  const dispatch = useDispatch();

  useEffect(() => {
      fetch(
        "https://jira.itglance.org/rest/api/3/search?jql=project=SIM",
        {
          method: "GET",
          auth: {
            user: "bot",
            pass: "botbotbot",
          },
        }
      )
        .then((response) => {
          console.log(response
          );
        })
        .then((text) => console.log(text))
        .catch((err) => console.error(err));
     setInterval(() => {
        var mill = Date.now() - localStorage.getItem("loginTime");
       let timeDisplay = `${Math.floor((mill % 86400000) / 3600000) ||
         "00"}:${Math.floor(mill / 60000 ) % 60 || "00"}:${Math.floor(
         mill / 1000
       ) % 60 || "00"}`;
       setTimeDisplay(timeDisplay);
     }, 1000);
   }, []);

  const addLog = async (e) => {
    e.preventDefault();
    dispatch(actions.addLog(logData));
     setLogscreenStatus(!logScreenStatus);
    setLogData({
      title: "",
      details: "",
      duration: "",
    });
   
  };

  const handleChange = (e) => {
    setLogData({
      ...logData,
      [e.target.name]: e.target.value,
    });
  };

   const handleLogout = () => {
     var total_time = Math.floor(
       (Date.now() - localStorage.getItem("loginTime")) / 1000
     );
     dispatch(actions.TimeTracker(total_time));
     localStorage.removeItem("loginTime");
     localStorage.removeItem("timerStopped");
     dispatch(action.logoutUser());
   };
  const changeAddLogScreen = () => { 
    setLogscreenStatus(!logScreenStatus);
  }


  const stopTimer = () => { 
    var total_time = Math.floor((Date.now() - localStorage.getItem("loginTime")) / 1000);
    dispatch(actions.TimeTracker(total_time));
    localStorage.setItem("loginTime", null)
    setTimeDisplay("00:00:00");
    setTimeStopped(true);
    localStorage.setItem("timerStopped", true)
  }

  const startTimer = () => { 
    setTimeStopped(false);
    localStorage.setItem("timerStopped", false);
    localStorage.setItem("loginTime", Date.now());
  }
  
  return (
    <div className="wrapper-padding">
      {localStorage.getItem("type") === 2 && <Redirect to="/admin/" />}
      <Toaster />
      <div
        className="timer"
        style={{
          backgroundColor: timerStopped ? "red" : "darkslategrey",
        }}
      >
        {timeDisplay}
      </div>
      <div
        className="btn secondary"
        style={{
          marginBottom: 10,
          marginTop: 10,
        }}
        onClick={timerStopped ? startTimer : stopTimer}
      >
        {timerStopped ? "Start TimerCard" : "Stop TimerCard"}
      </div>
      <div
        className="btn primary"
        style={{
          display: logScreenStatus && "none",
          marginBottom: 10,
          marginTop: 10,
        }}
        onClick={changeAddLogScreen}
      >
        Add Work Log
      </div>
      <div style={{ display: !logScreenStatus && "none" }}>
        <div className="title">Log your work time</div>
        <form onSubmit={addLog}>
          <div className="form-group">
            <div className="input-group">
              <label className="form-control">Title</label>
              <div className="input-box">
                <input
                  type="text"
                  name={"title"}
                  placeholder={"Title of the log"}
                  value={title}
                  onChange={handleChange}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <label className="form-control">Details</label>
              <div className="input-box">
                <textarea
                  name={"details"}
                  value={details}
                  placeholder={"Details of the log"}
                  onChange={handleChange}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="input-group">
              <label className="form-control">Duration</label>
              <div className="input-box">
                <input
                  type="number"
                  name={"duration"}
                  value={duration}
                  placeholder={"Enter the duration in minute"}
                  onChange={handleChange}
                />
              </div>
            </div>
          </div>
          <div className="form-button">
            <input type="submit" className="submit-button " value="Add Log" />
          </div>
          <div className="btn primary" onClick={changeAddLogScreen}>
            Hide Work Log Screen
          </div>
        </form>
      </div>
      <div className="btn danger" onClick={handleLogout}>
        Logout
      </div>
    </div>
  );
};

export default LogForm;
