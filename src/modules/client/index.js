import React from "react";
import Sidebar from "./component/Sidebar/Sidebar";
import ClientRoute from "./routes";
import './client.scss';
import ProfileImage from "../../assets/images/pika.jpg"
import {useSelector} from "react-redux";

const Client = () => {

    const {username} = useSelector(state => state.authenticationReducer.loginUser)

    return (
        <section className='client'>
            <Sidebar/>
            <div className="content-area">
                <div className="top-bar">
                    <div className='greeting'>Hi {username}!</div>
                    <div className="profile-image">
                        <img src={ProfileImage} alt=""/>
                    </div>
                </div>
                <div className='content'>
                    <ClientRoute/>
                </div>
            </div>
        </section>
    )
}

export default Client
