import * as actionTypes from './logType';

const initialState = {
    loading: false,
    unFinishTask: null,
    start_time: null,
    logById: null
};

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.LOADING:
            return {
                ...state,
                loading: true,
            };
        case actionTypes.UNFINISHED_TASK:
            return {
                ...state,
                unFinishTask: actions.unFinishTask,
                start_time: actions.unFinishTask?.start_time
            }
        case actionTypes.CLEAR_UNFINISHED_TASK:
            return {
                ...state,
                unFinishTask: null,
                start_time: null
            }
        case actionTypes.GET_LOGS_BY_ID:
            return {
                ...state,
                logById: actions.logById
            }
        default:
            return state;
    }
};
