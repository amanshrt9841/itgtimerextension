// import * as actionType from './logType';
import gql from 'graphql-tag';
import * as actions from "../../../store/actions";
import apolloClient from "../../../config/apolloclient";
import * as actionTypes from './logType';

export const getUnfinishedTaskSuccess = (unFinishedTask) => {
    return {
        type: actionTypes.UNFINISHED_TASK,
        unFinishTask: unFinishedTask
    }
}

export const clearUnfinishedTask = () => {
    return {
        type: actionTypes.CLEAR_UNFINISHED_TASK
    }
}

export const getLogsByIdSuccess = (logById) => {
    return {
        type: actionTypes.GET_LOGS_BY_ID,
        logById: logById
    }
}

export const addLog = (data) => async (dispatch) => {
    try {
        dispatch(actions.setLoading('log'));
        let userLog = await apolloClient.mutate({
            mutation: gql`
                mutation MyMutation($task : String!, $description :String, $project_id : bigint, $date: timestamptz!, $start_time: timestamptz!, $end_time: timestamptz, $user_id : Int!) {
                    insert_user_log(
                        objects: {
                            task: $task,
                            description: $description,
                            project_id: $project_id,
                            date: $date,
                            start_time: $start_time,
                            end_time: $end_time,
                            user_id: $user_id
                        }
                    ) {
                        affected_rows
                        returning {
                            id
                        }
                    }
                }
            `,
            variables: {
                task: data.task,
                description: data.description,
                project_id: data.project_id ? data.project_id : null,
                date: data.date,
                start_time: data.start_time,
                end_time: data.end_time ? data.end_time : null ,
                user_id: data.user_id,
            },
        });
        if (userLog) {
            dispatch(actions.startTimer())
            dispatch(getUnfinishedTask(data.user_id))
            dispatch(actions.setToasterState(
                true,
                "Success",
                "Log Added ",
                "Your work log is saved."
                )
            )
            dispatch(actions.clearLoading())
        }

    } catch (e) {
        console.log(e)
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                " Problem saving your log",
                "Logging Failed"
            )
        );
        dispatch(actions.clearLoading());
    }
};

export const setEndTimeOnLog = (id, end_time) => async (dispatch) => {
    apolloClient.mutate({
        mutation: gql`
            mutation MyMutation($id: Int!,$end_time: timestamptz!){
                update_user_log(where: {id: {_eq: $id}}, _set: {end_time: $end_time}) {
                    affected_rows
                }
            }
        `,
        variables: {
            id: id,
            end_time: end_time
        },
        update: (_cache, result) => {
            dispatch(actions.setToasterState(true, "Success", "Log Added ", "TimerCard Stopped."));
            dispatch(clearUnfinishedTask());
        }
    }).catch(error => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                " Problem saving your log",
                "Error in stopping time"
            )
        );
    })
}

export const updateUserLogById = (data,log_id) => async (dispatch) => {
    apolloClient.mutate({
        mutation: gql`
            mutation MyMutation($id: Int!,$task : String!, $description :String, $project_id : bigint, $date: timestamptz!, $start_time: timestamptz!, $end_time: timestamptz, $user_id : Int!){
                update_user_log(where: {id: {_eq: $id}},
                    _set: {
                        task: $task,
                        description: $description,
                        project_id: $project_id,
                        date: $date,
                        start_time: $start_time,
                        end_time: $end_time,
                        user_id: $user_id}
                ) {
                    affected_rows
                }
            }
        `,
        variables: {
            id: log_id,
            task: data.task,
            description: data.description,
            project_id: data.project_id,
            date: data.date,
            start_time: data.start_time,
            end_time: data.end_time? data.end_time : null,
            user_id: data.user_id,
        },
        update: (_cache, result) => {
            dispatch(actions.setToasterState(true, "Success", "Log Updated ", "user Log Updated"));
            dispatch(clearUnfinishedTask());
        }
    }).catch(error => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                " Problem Updating your log",
                "Problem Updating your log"
            )
        );
    })
}


export const TimeTracker = (data) => async (dispatch) => {
    try {
        dispatch(actions.setLoading("log"));
        console.log(data);
        let x = await apolloClient.mutate({
            mutation: gql`
                mutation MyMutation($timeTracked: Int!, $user_id: Int!) {
                    insert_time_tracker(
                        objects: { time_tracked: $timeTracked, user_id: $user_id }
                    ) {
                        affected_rows
                        returning {
                            time_tracked
                            id
                        }
                    }
                }
            `,
            variables: {
                timeTracked: data,
                user_id: localStorage.getItem("user"),
            },
        });
        if (x) {
            console.log(x);
            dispatch(
                actions.setToasterState(true, "Success", "Log Added ", "TimerCard Stopped.")
            );
            dispatch(actions.clearLoading());
        }
    } catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                " Problem saving your log",
                "Error in stopping time"
            )
        );
        dispatch(actions.clearLoading());
    }
};

export const getUnfinishedTask = (user_id) => async (dispatch) => {
    apolloClient.mutate({
        mutation: gql`
            query MyQuery($user_id: Int!){
                user_log(where: {end_time: {_is_null: true}, user_id: {_eq: $user_id}}) {
                    date
                    description
                    end_time
                    id
                    project_id
                    start_time
                    task
                    updated_at
                    created_at
                }
            }
        `,
        variables: {
            user_id: user_id
        },
        update: (_cache, result) => {
            dispatch(getUnfinishedTaskSuccess(result.data.user_log[0]))
        },
    }).catch(error => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem Fetching Unfinished Task",
                "Can't Fetch unfinished task"
            )
        );
    })
}

export const getLogsById = (logId) => async (dispatch) => {
    apolloClient.mutate({
        mutation: gql`
            query MyQuery($logId: Int!){
                user_log(where: {id: {_eq: $logId}}) {
                    id
                    date
                    description
                    end_time
                    project_id
                    start_time
                    task
                    time_spent
                    updated_at
                    created_at
                    project {
                        project_name
                    }
                }
            }
        `,
        variables: {
            logId: logId
        },
        update: (_cache, result) => {
            dispatch(getLogsByIdSuccess(result.data.user_log[0]))
        },
    }).catch(error => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem Fetching Log",
                "Can't Fetch Log"
            )
        );
    })
}

export const updateLog = (userlog) => async (dispatch) => {
    try {
        let x = apolloClient.mutate({
            mutation: gql`
                mutation MyMutation(
                    $logid: Int!
                    $detail: String!
                    $title: String!
                    $total_time: Int!
                    $project: String!
                ) {
                    update_user_log(
                        where: { id: { _eq: $logid } }
                        _set: { detail: $detail, title: $title, total_time: $total_time, project: $project }
                    ){
                        affected_rows
                        returning {
                            title
                        }}
                }
            `,
            variables: {
                logid: userlog.id,
                detail: userlog.detail,
                title: userlog.title,
                total_time: userlog.total_time,
                project: userlog.project
            },
        });
        return x;
    } catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem updating the data",
                "Log was not updated"
            )
        );
    }
}
