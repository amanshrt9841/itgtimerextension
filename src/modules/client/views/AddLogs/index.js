import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import "../../component/logform.scss";
import {getLogsById, getUnfinishedTask} from "../../services/logAction";
import './addLogs.scss';
import * as actions from "../../../../store/actions"
import {ValidateForm, ValidateInput} from "../../../../utills/validation";
import AddLogRule from "./AddLogRule";
import moment from "moment";
import {useParams} from "react-router-dom";
import {
    DatePicker,
    TimePicker,
    DateTimePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';

const AddLog = () => {

    const params = useParams();

    const {loginUser} = useSelector(state => state.authenticationReducer)
    const {unFinishTask} = useSelector(state => state.logReducer)
    const {logById} = useSelector(state => state.logReducer)
    const dispatch = useDispatch();

    const initialState = {
        task: "",
        description: "",
        project_id: null,
        start_time: "",
        end_time: null,
        date: "",
        user_id: ""
    }


    const [logData, setLogData] = useState(initialState);
    const [errors, setErrors] = useState(initialState);
    const [stateHasChanged, setStateHasChanged] = useState(true)

    const {projectsByUserId} = useSelector(state => state.adminReducer)

    const getProjects = () => {
        dispatch(actions.getProjectsByUserId());
    };

    useEffect(() => {
        getProjects();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (unFinishTask) {
            dispatch(actions.startTimer())

            setLogData({
                ...logData,
                task: unFinishTask.task,
                description: unFinishTask.description,
                project_id: unFinishTask.project_id
            })
        } else {
            setLogData({
                ...logData,
                task: "",
                description: "",
                project_id: null
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [unFinishTask]);

    useEffect(() => {
        if (logById) {
            setLogData({
                ...logData,
                task: logById.task,
                description: logById.description,
                project_id: logById.project_id,
                start_time: logById.start_time,
                end_time: logById.end_time,
                date: logById.date,
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [logById]);

    useEffect(() => {
        if (params.id) {
            dispatch(getLogsById(params.id))
        } else {
            dispatch(getUnfinishedTask(loginUser.id))
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);

    useEffect(() => {
        let curr = new Date();
        let date = curr.toISOString().substr(0, 10);
        setLogData(
            {
                ...logData,
                date: date,
                user_id: loginUser.id
            }
        )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [loginUser]);


    const {task, date, description, project_id} = logData;

    const handleChange = (e) => {
        setLogData({
            ...logData,
            [e.target.name]: e.target.value,
        });
        setStateHasChanged(false)
    };

    const inputValidation = (e) => {
        let errorMsg = ValidateInput(e.target.name, e.target.value, AddLogRule);
        setErrors({
            ...errors,
            [e.target.name]: errorMsg
        });
    }

    const startTimer = async (e) => {
        e.preventDefault();
        await dispatch(actions.getUnfinishedTask(loginUser.id))
        if (!unFinishTask) {
            let newLogData;
            let errorMsg = ValidateForm(logData, AddLogRule);
            setErrors({...errorMsg});
            let validated = Object.values(errorMsg).join('').length === 0;
            if (validated) {
                newLogData = {
                    ...logData,
                    start_time: new Date()
                };
                await setLogData(newLogData);
                params.id ? dispatch(actions.updateUserLogById(newLogData, params.id)) : dispatch(actions.addLog(newLogData))
            }
            setStateHasChanged(true)
        } else {
            dispatch(
                actions.setToasterState(
                    true,
                    "Error",
                    "Cant Add Log",
                    "New task cannot be added when there is unfinished task"
                )
            );
        }
    }

    const saveWorkLog = async (e) => {
        e.preventDefault();
        let newLogData;
        let errorMsg = ValidateForm(logData, AddLogRule);
        setErrors({...errorMsg});
        let validated = Object.values(errorMsg).join('').length === 0;
        if (validated) {
            newLogData = {
                ...logData,
            };
            await setLogData(newLogData);
            params.id ? dispatch(actions.updateUserLogById(newLogData, params.id)) : dispatch(actions.addLog(newLogData))
        }
        setStateHasChanged(true)
    }

    const startDateOnChangeHandler = (date) => {
        setLogData(
            {
                ...logData,
                start_time: date._d
            }
        )
    }

    const endDateOnChangeHandler = (date) => {
        setLogData({
            ...logData,
            end_time: date._d
        })
    }

    return (
        <section className='add-logs'>
            <div className="sub-title bold">Add Logs</div>
            <div className='division-line'></div>
            <form onSubmit={startTimer}>
                <div className="form-group d-flex justify-between">
                    <div className="input-group">
                        <label className="form-control label-color">Task</label>
                        <div className="input-box">
                            <input
                                type="text"
                                name={"task"}
                                placeholder={"Task"}
                                value={task}
                                onChange={(e) => {
                                    handleChange(e);
                                    inputValidation(e);
                                }}
                            />
                        </div>
                    </div>
                    <div className="input-group flex-1">
                        <label className="form-control label-color">Date</label>
                        <div className="input-box">
                            <p>{moment(date).format('MMM-DD-YYYY')}</p>
                        </div>
                    </div>
                </div>
                {errors.task !== '' ? <span className="error-text">{errors.task}</span> : ''}
                <div className="form-group">
                    <div className="input-group">
                        <label className="form-control label-color">Project</label>
                        <div className="input-box">
                            <select
                                name={"project_id"}
                                value={project_id}
                                onChange={(e) => {
                                    handleChange(e);
                                    inputValidation(e);
                                }}
                            >
                                <option value={null}>Select project</option>
                                {
                                    projectsByUserId.map((project, index) => (
                                        <option value={project.id} key={index}>{project.project_name}</option>
                                    ))
                                }
                            </select>
                        </div>
                    </div>
                </div>
                {errors.project !== '' ? <span className="error-text">{errors.project}</span> : ''}
                <div className="form-group">
                    <div className="input-group">
                        <label className="form-control label-color">
                            Description
                        </label>
                        <div className="input-box">
                <textarea
                    name={"description"}
                    value={description}
                    placeholder={"Details of the log"}
                    onChange={(e) => {
                        handleChange(e);
                        inputValidation(e);
                    }}
                    rows="3"
                />
                        </div>
                    </div>
                </div>
                {params.id && <div>
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        {/*<TimePicker value={startTime} onChange={setStartTime} />*/}
                        <div className='d-flex justify-between'>
                            <div className="form-group">
                                <div className="input-group">
                                    <label className="form-control label-color">Start Time</label>
                                    <DateTimePicker value={logData.start_time} onChange={startDateOnChangeHandler}/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <label className="form-control label-color">End Time</label>
                                    <DateTimePicker value={logData.end_time} onChange={endDateOnChangeHandler}/>
                                </div>
                            </div>
                        </div>
                    </MuiPickersUtilsProvider>
                </div>}
                {!params.id && <div className="form-button">
                    <button className="btn primary submit" disabled={stateHasChanged} onClick={startTimer}>
                        Start
                    </button>
                </div>}
                {params.id && <div className="form-button">
                    <button className="btn primary submit" onClick={saveWorkLog}>
                        Save
                    </button>
                </div>}
            </form>
        </section>
    );
};

export default AddLog;
