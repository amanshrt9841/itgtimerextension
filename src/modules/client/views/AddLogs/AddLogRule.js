export default {
    task: {
        required: true,
        label: 'Task'
    },
    project: {
        required: false,
        label: 'Project'
    }
}
