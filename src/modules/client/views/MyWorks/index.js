import React, {useEffect} from "react";
import "./myWork.scss";
import {FilterList} from "@material-ui/icons";
import * as actions from "../../../../store/actions";
import {useDispatch, useSelector} from "react-redux";
import Table from "../../../../common/Table";
import {useHistory} from "react-router-dom";

const Works = () => {
    const COLUMNS = [
        {
            name: 'title',
            align: 'left',
            label: 'Title',
            field: 'task',
            flexVal: 3,
            sortable: true,
        }, {
            name: 'project',
            align: 'left',
            label: 'Project',
            field: 'project',
            flexVal: 3,
            sortable: true,
        }, {
            name: 'date',
            align: 'center',
            label: 'date',
            field: 'date',
            flexVal: 2,
            sortable: true,
        }, {
            name: 'time_spent',
            align: 'center',
            label: 'Time Spent',
            field: `time_spent`,
            flexVal: 2,
            sortable: true,
        }
    ];

    const dispatch = useDispatch();
    const history = useHistory();

    const {userLogsByUserId} = useSelector(state => state.adminReducer)

    const getProjects = () => {
        dispatch(actions.getUserLogsByUserId());
    };

    useEffect(() => {
        getProjects();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const viewAction = (log) => {
        history.push(`my-work/log/${log.id}`);
    }

    const editAction = (log) => {
        history.push(`my-work/edit/${log.id}`);
    }

    return (
        <section className='my-work'>
            <div className='d-flex justify-between'>
                <div className="sub-title bold">
                    My works
                </div>
                <FilterList/>
            </div>
            <div className='division-line'></div>
            <Table
                columns={COLUMNS}
                rows={userLogsByUserId}
                limit={8}
                viewAction={viewAction}
                editAction={editAction}
            />
        </section>
    );
};

export default Works;
