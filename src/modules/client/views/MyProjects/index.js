import React, {useEffect} from "react";
import "./myProject.scss";
import ProjectCard from "../../../../common/ProjectCard";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom'
import * as actions from "../../../../store/actions";

const Projects = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const { projectsByUserId } = useSelector(state => state.adminReducer)


    const getProjects = () => {
        dispatch(actions.getProjectsByUserId());
    };

    useEffect(() => {
        getProjects();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleProjectClick = (project_id) => {
        history.push(`/client/my-projects/${project_id}`);
    }

    return (
        <section className='projects'>
            <div className="sub-title bold">My Projects</div>
            <div className='division-line'></div>
            <div className='project-list'>
                {
                    projectsByUserId.map((project,keys) => (
                       <div className='project-item' key={keys} onClick={() => {handleProjectClick(project.id)}}>
                           <ProjectCard
                               projectName={project.project_name}
                               projectDuration={project.project_duration || "Loading..."}
                               durationUnit={project.project_duration || ""}
                           />
                       </div>
                    ))
                }
            </div>

        </section>
    );
};

export default Projects;
