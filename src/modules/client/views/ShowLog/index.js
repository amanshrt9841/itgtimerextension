import React, {useEffect} from "react";
import './showLog.scss';
import {useDispatch, useSelector} from "react-redux";
import moment from "moment";
import {useParams} from "react-router-dom";
import * as action from "../../../../store/actions";

const ShowLog = () => {
    const params = useParams();
    const log_id = params.id;

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(action.getLogsById(log_id))
    }, [])

    const {logById} = useSelector(state => state.logReducer)

    return (
        <section className='show-log'>
            <div className='d-flex'>
                <div className="sub-title bold">
                    My works
                </div>
            </div>
            <div className='division-line'></div>
            <div className='d-flex justify-between info-div'>
                <div className='d-flex flex-column flex-1'>
                    <p className='primary bold primary-text'>Title</p>
                    <p className='description'>{logById && logById.task}</p>
                </div>
                <div className='d-flex flex-column flex-1'>
                    <p className='primary bold primary-text'>date</p>
                    <p className='description'>{logById && moment(logById.date).format('MMM-DD-YYYY')}</p>
                </div>
            </div>
            <div className='d-flex flex-column info-div'>
                <p className='primary bold primary-text'>Project</p>
                <p className='description'>{logById && logById.project?.project_name ? logById.project.project_name : "No Project Assigned"}</p>
            </div>
            <div className='d-flex flex-column info-div'>
                <p className='primary bold primary-text'>Description</p>
                <p className='description'>{logById && logById.description ? logById.description : "No description"}</p>
            </div>
            <div className='d-flex flex-column info-div'>
                <p className='primary bold primary-text'>Duration</p>
                <p className='description clock'>{logById && logById.time_spent.toString().slice(0,8)}</p>
            </div>
        </section>
    )
}

export default ShowLog;
