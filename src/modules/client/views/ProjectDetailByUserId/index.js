import React, {useEffect} from "react";
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import * as actions from "../../../../store/actions";
import './projectDetailByUserId.scss';
import Table from "../../../../common/Table";


const ProjectDetailByUserId = () => {

    const params = useParams();
    const project_id = params.id;

    const { projectLogsByUserId } = useSelector(state => state.adminReducer)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(actions.getProjectLogsByProjectIdAndUserId(project_id));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const COLUMNS = [
        {
            name: 'sn',
            align: 'center',
            label: 'SN',
            field: 'id',
            flexVal: 1,
            sortable: true,
        }, {
            name: 'title',
            align: 'left',
            label: 'Title',
            field: 'task',
            flexVal: 2,
            sortable: true,
        }, {
            name: 'date',
            align: 'left',
            label: 'date',
            field: 'date',
            flexVal: 2,
            sortable: true,
        }, {
            name: 'time_spent',
            align: 'left',
            label: 'Time Spent',
            field: `time_spent`,
            flexVal: 2,
            sortable: true,
        }
    ];


    return (
        <section className='projects'>
            <div className="sub-title bold">Project Description</div>
            <div className='division-line'></div>
            <Table
                columns={COLUMNS}
                rows={projectLogsByUserId}
                limit={7}
            />
        </section>
    )
}

export default ProjectDetailByUserId;
