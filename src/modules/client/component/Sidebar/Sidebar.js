import React from "react";
import {Assessment, Assignment, ExitToApp, NoteAdd} from "@material-ui/icons";
import {NavLink} from "react-router-dom";
import * as actions from "../../services/logAction";
import {useDispatch} from "react-redux";
import * as action from "../../../../store/actions";
import TimerCard from "../TimerCard";

const Sidebar = () => {
    const menuItems = [
        {icons: <NoteAdd/>, label: 'Add Logs', link: '/add-logs'},
        {icons: <Assignment/>, label: 'My Works', link: '/my-work'},
        {icons: <Assessment/>, label: 'My Projects', link: '/my-projects'},
    ]

    const dispatch = useDispatch();

    // const getProjectsList = async () => {
    //     let header = {
    //         Accept: "application/json",
    //         Authorization: "Basic Ym90OmJvdGJvdGJvdA==",
    //     };
    //     const result = await axios.get(
    //         "https://jira.itglance.org/rest/api/2/project",
    //         {
    //             headers: header
    //         }
    //     );
    //     console.log("thi is reulat", result);
    // }

    const handleLogout = () => {
        let total_time = Math.floor(
            (Date.now() - localStorage.getItem("loginTime")) / 1000
        );
        dispatch(actions.TimeTracker(total_time));
        localStorage.removeItem("loginTime");
        localStorage.removeItem("timerStopped");
        dispatch(action.logoutUser());
        window.location.reload(true);
    };

    return (
        <div className='sidebar'>
            <div className='logo'>
                <p className='sub-title'>Project Planner</p>
                <p className='thin-text'>Track your workflow</p>
            </div>
            <div className='menu'>
                <p>Menu</p>
                <div className='menu-items'>
                    {
                        menuItems.map((item, key) => (
                            <NavLink to={`/client${item.link}`} className='menu-item pointer' exact={item.link === ''}
                                     key={key}>
                                {item.icons}
                                <label className='menu-label pointer'>{item.label}</label>
                            </NavLink>
                        ))
                    }
                </div>
            </div>
            <TimerCard/>
            <div className='logout pointer'
                 onClick={handleLogout}
            >
                <ExitToApp className='icon'/>
                <span>Log Out</span>
            </div>
        </div>
    )
}

export default Sidebar
