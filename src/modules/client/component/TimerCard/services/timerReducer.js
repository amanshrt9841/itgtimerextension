import * as actionTypes from './timerTypes';
import {TimerStatus} from "../../../../../enum/TimerStatus";

const initialState = {
    timerStatus: TimerStatus.stop,
};

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.TIMER_STATUS:
            return {
                ...state,
                timerStatus: actions.timerStatus,
            };
        default:
            return state;
    }
};
