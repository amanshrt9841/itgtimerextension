import * as actionType from './timerTypes'
import {TimerStatus} from "../../../../../enum/TimerStatus";

const setTimerStatus = (timerStatus) => {
    return {
        type: actionType.TIMER_STATUS,
        timerStatus: timerStatus
    }
}

export const startTimer = () => dispatch =>  {
   dispatch(setTimerStatus(TimerStatus.start))
}

export const stopTimer = () => dispatch => {
   dispatch(setTimerStatus(TimerStatus.stop))
}
