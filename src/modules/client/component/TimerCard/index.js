import React, {useEffect, useState} from "react";
import {Stop} from "@material-ui/icons";
import './timerCard.scss'
import * as actions from "./services/timerAction";
import {useDispatch, useSelector} from "react-redux";
import {TimerStatus} from "../../../../enum/TimerStatus";
import moment from "moment";
import {setEndTimeOnLog} from "../../services/logAction";

const TimerCard = () => {

    const dispatch = useDispatch();
    const initialTimerState = '00:00:00'
    const [timerState, setTimerState] = useState(initialTimerState);

    const {timerStatus} = useSelector(state => state.timerReducer)
    const {start_time} = useSelector(state => state.logReducer)
    const {unFinishTask} = useSelector(state => state.logReducer)

    const stopTimer = () => {
        dispatch(actions.stopTimer());
        let now = new Date().toISOString()
        dispatch(setEndTimeOnLog(unFinishTask.id, now))
    }

    useEffect(() => {
        console.log((timerStatus === TimerStatus.start && start_time))
        console.log((timerStatus === TimerStatus.stop))
        let timerInterval
        if (timerStatus === TimerStatus.start && start_time) {
            timerInterval = setInterval(() => {
                let now = Date.now()
                setTimerState(moment.utc(moment(now).diff(moment(start_time))).format("HH:mm:ss"));
            }, 1000);
        }
        return () => clearInterval(timerInterval);
    }, [timerStatus, start_time])

    useEffect(() => {
        if (timerStatus === TimerStatus.stop) {
            console.log("Stopped")
            setTimerState(initialTimerState)
        }
    }, [timerStatus]);


    return (
        <div className='timer-section'>
            <p>Time Spent</p>
            <div className='clock'>
                <div className="timer">
                    <div>{timerState.split(":")[0]}</div>
                    <div className='time-unit'>hr</div>
                </div>
                <div className="timer">
                    <div>{timerState.split(":")[1]}</div>
                    <div className='time-unit'>min</div>
                </div>
                <div className="timer">
                    <div>{timerState.split(":")[2]}</div>
                    <div className='time-unit'>sec</div>
                </div>
            </div>
            {(timerStatus === TimerStatus.start) ?
                (<div className="btn white primary-text" onClick={stopTimer}>
                        <div className='btn-content'>
                            {" "}
                            <Stop/> <span>Stop timer</span>{" "}
                        </div>
                    </div>
                ) : null
            }
        </div>
    )
}

export default TimerCard;