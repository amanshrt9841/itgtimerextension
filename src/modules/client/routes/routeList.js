import Works from "../views/MyWorks";
import Projects from "../views/MyProjects";
import ProjectDetailByUserId from "../views/ProjectDetailByUserId";
import ShowLog from "../views/ShowLog";
import AddLog from "../views/AddLogs";

export default [
    {
        path: '/add-logs',
        name: 'AddLogs',
        component: AddLog,
        exact: true
    },
    {
        path: '/my-work',
        name: 'MyWork',
        component: Works,
        exact: true
    },
    {
        path: '/my-work/log/:id',
        name: 'ViewLogs',
        component: ShowLog,
        exact: true
    },{
        path: '/my-work/edit/:id',
        name: 'EditLogs',
        component: AddLog,
        exact: true
    },
    {
        path: '/my-projects',
        name: 'MyProjects',
        component: Projects,
        exact: true
    },
    {
        path: "/my-projects/:id",
        name: "admin.project-details",
        component: ProjectDetailByUserId,
        exact: true,
    },
]
