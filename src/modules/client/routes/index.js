import React from "react";
import RouteLists from './routeList'
import {Route, Switch} from "react-router-dom";

const ClientRoute = () => {
    return (
        <>
            <Switch>
                {
                    RouteLists.map((route,key) => (
                        <Route path={`/client${route.path}`} name={route.name} component={route.component}
                               exact={route.exact} key={key}/>
                    ))
                }
            </Switch>

        </>
    )
}

export default ClientRoute;