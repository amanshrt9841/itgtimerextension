import React from 'react';
import {NavLink} from "react-router-dom";
import {ExitToApp, Forum, Home, Notifications, Person, ShowChart, SupervisorAccount, Work} from "@material-ui/icons";

const Sidebar = ({handleLogout}) => {
    const sidebarItems = [
        {icons: <Home />, label: 'Dashboard', link: ''},
        {icons: <Person />, label: 'Users', link: '/users'},
        {icons: <SupervisorAccount />, label: 'Admins', link: '/admin-users'},
        {icons: <Work />, label: 'Projects', link: '/projects'},
        {icons: <ShowChart />, label: 'Performance Analysis', link: '/performance'},
        {icons: <Forum />, label: 'Message', link: '/messages'},
        {icons: <Notifications />, label: 'Notifications', link: '/notifications'},
    ]

    return (
        <div className="sidebar">
            <div className="logo">
                <div className="sub-title">ITG Timer</div>
                <div className="thin-text">Administration</div>
            </div>

            <div className="menu">
                <p>Menu</p>
                <div className='menu-items'>
                    {
                        sidebarItems.map((item, key) => (
                            <NavLink to={`/admin${item.link}`} className='menu-item pointer' exact={item.link === ''}
                                     key={key}>
                                <div className="sidebar-icon">
                                    {item.icons}
                                </div>
                                <label className='menu-label pointer'>{item.label}</label>
                            </NavLink>
                        ))
                    }
                </div>

            </div>

            <div className='logout pointer'
                 onClick={handleLogout}
            >
                <ExitToApp className='icon'/>
                <span>Log Out</span>
            </div>
        </div>
    )
}

export default Sidebar;
