import React from 'react';
import SearchIcon from "../../../../assets/icons/Search.svg";
import NotificationIcon from "../../../../assets/icons/Notification.svg";
import UserImage from "../../../../assets/images/pika.jpg";

const Navbar = ({ selectedTab }) => {
    return (
      <div className="nav-bar">
        <div className="selected-tab">{selectedTab}</div>
        <div className="nav-items">
          <div className="search-bar">
            <input name="search-bar" placeholder="Search" />
            <img src={SearchIcon} alt="search" className="search-btn" />
          </div>

          <div className="notification">
            <img src={NotificationIcon} alt="notification" />
          </div>

          <div className="user-profile">
            <img src={UserImage} alt="user-profile" />
          </div>
        </div>
      </div>
    );
}

export default Navbar;