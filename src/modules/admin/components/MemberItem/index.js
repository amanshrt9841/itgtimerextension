import React from 'react';
import './memberItem.scss';
import {AddBox, DeleteOutline} from '@material-ui/icons';
import defaultUserImage from '../../../../assets/images/pika.jpg';
import {MemberCardType} from "../../../../enum/MemberCardType";

const MemberItem = (props) => {
    const {id, firstName, lastName, username, type, addMember, deleteMember, closeAddMemberModal} = props;

    return (
        <div className="member-item d-flex justify-between align-center">
            <div className="d-flex align-center">
                <div className="image-container">
                    <img src={defaultUserImage} alt="user"/>
                </div>
                <div className="member-info">
                    <div>{firstName} {lastName}</div>
                    <div className="member-username">{username}</div>
                </div>
            </div>
            {type === MemberCardType.DELETE && deleteMember &&
            <div className="pointer ml-md" onClick={() =>
                deleteMember(id)}><DeleteOutline/></div>}
            {type === MemberCardType.ADD && <div className="pointer ml-md" onClick={() => {
                addMember(id)
                closeAddMemberModal()
            }}><AddBox/></div>}
        </div>
    )
}

export default MemberItem;
