import * as actionTypes from '../actions/adminActionType';

const initialState = {
    adminUserList: [],
    userList: [],
    userById: {},
    projects: [],
    projectsByUserId: [],
    projectLogs: [],
    projectLogsByUserId: [],
    userLogs: [],
    userLogsByUserId: [],
    calculatedProjectLogs: [],
    projectAdminMembers: [],
    projectUserMembers: []
}

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.GET_ADMIN_USERS:
            return {
                ...state,
                adminUserList: actions.payload
            };

        case actionTypes.GET_USERS:
            return {
                ...state,
                userList: actions.payload
            }
        case actionTypes.GET_USER_BY_ID:
            return {
                ...state,
                userById: actions.userById
            }
        case actionTypes.GET_PROJECTS:
            return {
                ...state,
                projects: actions.payload
            }
        case actionTypes.GET_PROJECTS_BY_USER_ID:
            return {
                ...state,
                projectsByUserId: actions.payload
            }
        case actionTypes.GET_PROJECT_LOGS:
            return {
                ...state,
                projectLogs: actions.payload
            }
        case actionTypes.GET_PROJECT_LOGS_BY_PROJECT_ID_AND_USER_ID:
            return {
                ...state,
                projectLogsByUserId: actions.payload
            }
        case actionTypes.GET_USER_LOGS:
            return {
                ...state,
                userLogs: actions.payload
            }
        case actionTypes.GET_USER_LOGS_BY_USER_ID:
            return {
                ...state,
                userLogsByUserId: actions.payload
            }
        case actionTypes.GET_CALCULATED_PROJECT_LOGS:
            // let distinctLogs = actions.payload.map((log) => {
            //     console.log(log)
            // });

            return {
                ...state,
                calculatedProjectLogs: actions.payload
            }
        case actionTypes.GET_PROJECT_ADMIN_MEMBERS:
            return {
                ...state,
                projectAdminMembers: actions.payload
            }
        case actionTypes.GET_PROJECT_USER_MEMBERS:
            return {
                ...state,
                projectUserMembers: actions.payload
            }
        default:
            return state
    }
}
