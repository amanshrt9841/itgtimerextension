import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import * as actions from "./../actions/dashboardActions";
import '../dashboard.scss';

import ListTable from '../../../common/ListTable';

import DefaultProfileImage from '../../../assets/images/default-user.png';

const COLUMNS = [
  {
      name: 'sn',
      align: 'center',
      label: 'SN',
      field: 'sn',
      flexVal: 1,
      sortable: true,
  }, {
      name: 'project_name',
      align: 'left',
      label: 'Project Name',
      field: 'project_name',
      flexVal: 2,
      sortable: true,
  }, {
      name: 'number_of_workers',
      align: 'center',
      label: 'Number of Workers',
      field: 'number_of_workers',
      flexVal: 2,
      sortable: true
  }, {
      name: 'total_time_spent',
      align: 'center',
      label: 'Total Time Spent',
      field: 'total_time_spent',
      flexVal: 2,
      sortable: true
  }
];

const ROWS = [
  {
    sn: 1,
    project_name: "Facebook",
    number_of_workers: 20,
    total_time_spent: 50
  },
  {
    sn: 2,
    project_name: "Google",
    number_of_workers: 30,
    total_time_spent: 100
  },
  {
    sn: 3,
    project_name: "Facebook",
    number_of_workers: 20,
    total_time_spent: 50
  },
  {
    sn: 4,
    project_name: "Google",
    number_of_workers: 30,
    total_time_spent: 100
  },
  {
    sn: 5,
    project_name: "Facebook",
    number_of_workers: 20,
    total_time_spent: 50
  }
];

const WORKER_COLUMNS = [
  {
      name: 'sn',
      align: 'center',
      label: 'SN',
      field: 'sn',
      flexVal: 1,
      sortable: true,
  }, {
      name: 'worker_name',
      align: 'left',
      label: 'Name',
      field: 'worker_name',
      flexVal: 2,
      sortable: true,
  }, {
      name: 'hours',
      align: 'center',
      label: 'Hours',
      field: 'hours',
      flexVal: 2,
      sortable: true
  }
];

const WORKER_ROWS = [
  {
    sn: 1,
    worker_name: "Jenny Wilson",
    hours: 20
  },
  {
    sn: 2,
    worker_name: "Wade Warren",
    hours: 10
  },
  {
    sn: 3,
    worker_name: "Wilson Warren",
    hours: 10
  },
  {
    sn: 4,
    worker_name: "Jenny Warren",
    hours: 10
  },
  {
    sn: 5,
    worker_name: "Jenny Warren",
    hours: 10
  }
];

const Dashboard = (props) => {
  const [usersCount, setUsersCount] = useState("0");
  const [usersHours, setUsersHours] = useState("0");
  const dispatch = useDispatch();
  useEffect(() => {
    getUsersCount();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getUsersCount = async() => { 
    let x = await dispatch(actions.getUsers());
    setUsersCount(x.data.users.length);
    // let time = await dispatch(actions.getUsersHours());
    let timeArray = [];
    // time.data.time_tracker.map((t) => timeArray.push(t.time_tracked));
    let totalInSeconds = timeArray.reduce((a, b) => a + b, 0);
    setUsersHours(((totalInSeconds % 86400000) / 3600000).toFixed(2));
  }
  
  return (
    <div className="dashboard-screen">
      <div className="worker-project">
        <div className="worker-time d-flex flex-1 flex-row justify-around">
          <div className="info-bar">
            <span>Total Users</span>
            <span style={{ fontSize: 60, marginTop: 30 }}>{usersCount}</span>
          </div>
          <div className="info-bar">
            <span>Total Users Hours</span>
            <span style={{ fontSize: 60, marginTop: 30 }}>
              {usersHours}
            </span>
          </div>
        </div>

        <div className="running-projects list-table-section flex-1">
          <div className="card-title">Running Projects</div>
          <ListTable
            rows={ROWS}
            columns={COLUMNS}
            limit={5}
          />
          <div className="view-all-btn">
            <a href="/adminDash">View All Projects</a>
          </div>
        </div>

        <div className="this-week card">
          This Week
        </div>
      </div>

      <div className="worker-list d-flex flex-column">
        <div className="top-workers list-table-section flex-1">
          <div className="card-title">Top Workers</div>
          <ListTable
            rows={WORKER_ROWS}
            columns={WORKER_COLUMNS}
            limit={5}
          />
          <div className="view-all-btn">
            <a href="/adminDash">View All Workers</a>
          </div>
        </div>

        <div className="best-worker card d-flex flex-column">
          <div className="card-title">Last Month's Best Worker</div>
          <div className="d-flex align-center">
            <div className="best-worker-image">
              <img src={DefaultProfileImage} alt="user" />
            </div>
            <div className="best-worker-info">
              <div className="worker-info">Wade Warren</div>
              <div className="worker-info">87 Hours</div>
            </div>
          </div>
        </div>

        <div className="average-hours card flex-1">
          <div>Average Hours</div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
