import React, { useState } from "react";
import { useDispatch } from "react-redux";
import * as actions from "../../../store/actions";
import Toaster from "../../../common/Toaster/Toaster";
import InputBox from "../../../common/InputBox";
import { useHistory } from "react-router-dom";

const ProjectForm = () => {
  const dispatch = useDispatch();
  const [project, setProject] = useState({});
  const history = useHistory();

  const handleChange = (e) => {
    e.persist();
    setProject({
        ...project,
        [e.target.name]: e.target.value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let res = await dispatch(actions.addProject(project));
    if (res === "success") history.push('/admin/projects');
  };

  return (
    <div className="main-body d-flex flex-column justify-between">
        <div className="add-project-form">
          <Toaster/>
          <div className="form-title">
            Add Project
          </div>
          <form className="d-flex flex-column justify-between" onSubmit={e => handleSubmit(e)}>
            <div>
                <div className="form-group d-flex">
                    <InputBox
                        type="text"
                        label="Project Name"
                        name="project_name"
                        placeholder="Enter Project Name"
                        onChange={handleChange}
                        required={true}
                    />
                    <InputBox
                        type="date"
                        label="Create Date"
                        name="created_at"
                        onChange={handleChange}
                        required={true}
                    />
                </div>
                <div className="form-group d-flex">
                    <InputBox
                        inputType="textarea"
                        rows={15}
                        label="Project Description"
                        name="description"
                        placeholder="Enter Project Description"
                        onChange={handleChange}
                        required={true}
                    />
                </div>
            </div>
            <div className="d-flex justify-end">
                <button className="btn primary" type="submit">
                    Save
                </button>
            </div>
          </form>
        </div>
    </div>
  );
};

export default ProjectForm;
