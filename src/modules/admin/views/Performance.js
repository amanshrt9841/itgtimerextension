import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import * as actions from "../actions/dashboardActions";
import Modal from "react-modal";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
        transform: "translate(-50%, -50%)",
    backgroundColor : "black"
  },
};

const Performance = (props) => {
    const [emails, setEmails] = useState([]);
  const  [worksList, setWorksList] = useState([]);
    const [hoursTracked, setHoursTracked] = useState([]);
    const dispatch = useDispatch();
    var subtitle;
    const [modalIsOpen, setIsOpen] = React.useState(false);
    function openModal() {
      setIsOpen(true);
    }

    function afterOpenModal() {
      // references are now sync'd and can be accessed.
      subtitle.style.color = "#f00";
    }

    function closeModal() {
      setIsOpen(false);
    }
  useEffect(() => {
    getUsersEmail();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getUsersEmail = async () => {
      let x = await dispatch(actions.getUsers());
      setEmails(x.data.users);
  };

    const checkuser = async (id) => {
        let worksAccomplished = await dispatch(actions.getWorksAccomplished(id));
      // let HoursTracked = await dispatch(actions.getHourstrackedPerworker(id));
      // worksList = worksAccomplished.data.user_log;
      let worksArray = [];
      worksAccomplished.data.user_log.map((works) => {
        worksArray.push(works.detail)
      }
      );
      setWorksList(["a", worksArray]);
        setHoursTracked(worksAccomplished.data.time_tracker);
        setIsOpen(true);
    }

  return (
    <div className="main-body">
      <div className="upper-dash">
        <div className="model-background">
          <Modal
            isOpen={modalIsOpen}
            onAfterOpen={afterOpenModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Example Modal"
          >
            <h2 ref={(_subtitle) => (subtitle = _subtitle)}>User Analysis</h2>
            <div className="upper-dash">
              <div className="info-bar-model">
                <span>Average Hours worked per day</span>
                <span style={{ fontSize: 60, marginTop: 30 }}>10</span>
              </div>
              <div className="info-bar-model">
                <span>Total Hours worked this week</span>
                <span style={{ fontSize: 60, marginTop: 30 }}>100</span>
              </div>
            </div>
            {/* <h2 >
              Accomplished Works this week
            </h2>
            <div className="upper-dash" >
              {worksList && worksList.map(works => (<div style={{color : "white"}}>{works}</div>))}
            </div> */}
            <button onClick={closeModal}>close</button>
          </Modal>
        </div>
        <div style={{ display: "flex", flexDirection: "column" }}>
          {emails.map((email, key) => (
            <div className="emails-row">
              <span> {`${key + 1} .${email.email}`}</span>
              <div
                className="analyze-link"
                onClick={() => checkuser(email.id)}
              >
                Analyze
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Performance;
