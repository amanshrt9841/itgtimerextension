import React, { useState, useEffect } from 'react';
import ProjectCard from '../../../common/ProjectCard';
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../../../store/actions";
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";

const Projects = () => {
    const [projects, setProjects] = useState([]);
    const dispatch = useDispatch();
    const history = useHistory();
    const allProjects = useSelector((state) => {
        return state.adminReducer.projects;
    });

    const getProjects = () => {
        dispatch(actions.getProjects());
    };

    const getCalculatedProjectLogs = () => {
        dispatch(actions.getCalculatedProjectLogs());
    };

    const handleProjectClick = (project_id) => {
        history.push(`/admin/projects/${project_id}`);
    }

    useEffect(() => {
        getProjects();
        getCalculatedProjectLogs();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


    useEffect(() => {
        setProjects(allProjects);
        // allProjects.forEach(project => {

        // });
    }, [allProjects]);


    return (
        <div className="main-body">
            <div className="admin-projects">
                <div className="page-title-section">
                    <div className="page-title">Projects</div>
                    <Link to="/admin/projects/add" className="primary-link">
                        <div className="btn primary">Add Project +</div>
                    </Link>
                </div>
                <div className="project-lists">
                    {
                        projects &&
                        projects.map((project) => (
                            <div className="project-item" onClick={() => {handleProjectClick(project.id)}}>
                                <ProjectCard
                                    projectName={project.project_name}
                                    projectDuration={project.project_duration || "Loading..."}
                                    durationUnit={project.project_duration || ""}
                                />
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    );
}

export default Projects;
