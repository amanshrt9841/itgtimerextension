import React, {useState, useEffect} from 'react';
import Table from '../../../common/Table';
import {Link, useHistory} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import * as actions from "../../../store/actions";

const COLUMNS = [
    {
        name: 'sn',
        align: 'center',
        label: 'SN',
        field: 'id',
        flexVal: 1,
        sortable: true,
    }, {
        name: 'first_name',
        align: 'left',
        label: 'First Name',
        field: 'first_name',
        flexVal: 2,
        sortable: true,
    }, {
        name: 'last_name',
        align: 'left',
        label: 'Last Name',
        field: 'last_name',
        flexVal: 2,
        sortable: true,
    }, {
        name: 'username',
        align: 'left',
        label: 'Username',
        field: 'username',
        flexVal: 2,
        sortable: true,
    }, {
        name: 'email',
        align: 'left',
        label: 'Email',
        field: 'email',
        flexVal: 2,
        sortable: true,
    }
];

const UserList = () => {
    const [userList, setUserList] = useState([]);
    const history = useHistory();
    const dispatch = useDispatch();
    const adminReducer = useSelector((state) => {
        return state.adminReducer.userList;
    });

    const getUsers = () => {
        dispatch(actions.getUsers());
    }

    const viewAction = (obj) => {
        console.log("VIEW");
        console.log(obj);
    }

    const editAction = (obj) => {
        history.push(`/admin/users/edit/${obj.id}`)
    }

    const deleteAction = (obj) => {
        console.log(obj)
    }

    useEffect(() => {
        getUsers();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setUserList(adminReducer);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [adminReducer]);

    return (
        <div className="main-body">
            <div className="user-list">
                <div className="page-title-section">
                    <div className="page-title">Users</div>
                    <Link to="/admin/users/add" className="primary-link">
                        <div className="btn primary">Add User +</div>
                    </Link>
                </div>
                <Table
                    columns={COLUMNS}
                    rows={userList}
                    limit={10}
                    viewAction={viewAction}
                    editAction={editAction}
                    deleteAction={deleteAction}
                />
            </div>
        </div>
    )
}

export default UserList;
