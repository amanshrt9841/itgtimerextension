import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as actions from "../../../store/actions";
import Toaster from "../../../common/Toaster/Toaster";
import InputBox from "../../../common/InputBox";
import {useHistory, useParams} from "react-router-dom";


const initialState = {
    first_name: '',
    last_name: '',
    email: '',
    username: '',
    password: ''
}

const AddAdminUser = () => {
    const dispatch = useDispatch();
    const [user, setUser] = useState(initialState);
    const history = useHistory();
    const userId = useParams().id;

    const userById = useSelector(state => state.adminReducer.userById)

    useEffect(() => {
        if (userId) {
            dispatch(actions.getUserById(userId))
        }
    }, [userId])

    useEffect(() => {
        if (userById && userId) {
            setUser({
                ...user,
                first_name: userById.first_name,
                last_name: userById.last_name,
                email: userById.email,
                username: userById.username
            })
        }
    }, [userById, userId])

    const handleChange = (e) => {
        e.persist()
        setUser({
            ...user,
            [e.target.name]: e.target.value
        });
    };

    const handleSubmit = async () => {
        let userWithUserType = {
            ...user,
            type: "admin"
        }
        let res;
        if (userId) {
            res = await dispatch(actions.updateUser(userWithUserType, userId));
        } else {
            res = await dispatch(actions.addUser(userWithUserType));
        }
        if (res === "success") history.push('/admin/admin-users');
    };

    return (
        <div className="main-body d-flex flex-column justify-between">
            <div className="add-user-form">
                <Toaster/>
                <div className="form-title">
                    Add Admin
                </div>
                <div className="form-group d-flex">
                    <InputBox
                        type="text"
                        label="First Name"
                        name="first_name"
                        value={user.first_name}
                        placeholder="Enter First Name"
                        onChange={handleChange}
                    />
                    <InputBox
                        type="text"
                        label="Last Name"
                        name="last_name"
                        value={user.last_name}
                        placeholder="Enter Last Name"
                        onChange={handleChange}
                    />
                </div>
                <div className="form-group d-flex">
                    <InputBox
                        type="email"
                        label="Email"
                        name="email"
                        value={user.email}
                        placeholder="Enter Email Address"
                        onChange={handleChange}
                    />
                </div>
                <div className="form-group d-flex">
                    <InputBox
                        type="text"
                        label="Username"
                        name="username"
                        value={user.username}
                        placeholder="Enter Username"
                        onChange={handleChange}
                    />
                    <InputBox
                        type="password"
                        label="Password"
                        name="password"
                        value={user.password}
                        placeholder="Enter Password"
                        onChange={handleChange}
                    />
                </div>
            </div>
            <div className="d-flex justify-end">
                <div className="btn primary" onClick={handleSubmit}>
                    Save
                </div>
            </div>
        </div>
    );
};

export default AddAdminUser;
