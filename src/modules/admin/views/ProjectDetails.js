import React, {useEffect, useState} from 'react';
import * as actions from "../../../store/actions";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from 'react-router-dom';
import TaskCard from '../../../common/TaskCard';
import MemberItem from '../components/MemberItem';
import {UserType} from '../../../enum/UserType';
import {Add, Close} from '@material-ui/icons';
import Modal from 'react-modal';
import {MemberCardType} from "../../../enum/MemberCardType";

const ProjectDetails = () => {
    const params = useParams();
    const project_id = params.id;

    const [projectLogs, setProjectLogs] = useState([]);
    const [addMembersModalOpen, setAddMembersModalOpen] = useState(false)

    const openAddMembersModal = () => {
        dispatch(actions.getUsers());
        setAddMembersModalOpen(true);
    }

    const closeAddMemberModal = () => {
        setAddMembersModalOpen(false);
    }

    const dispatch = useDispatch();
    const allProjectLogs = useSelector((state) => {
        return state.adminReducer.projectLogs;
    });

    const userMembers = useSelector((state) => {
        return state.adminReducer.projectUserMembers;
    });

    const users = useSelector((state) => {
        return state.adminReducer.userList
    })

    const adminMembers = useSelector((state) => {
        return state.adminReducer.projectAdminMembers;
    });

    useEffect(() => {
        dispatch(actions.getProjectLogs(project_id));
        dispatch(actions.getProjectMembers(project_id, UserType.user));
        dispatch(actions.getProjectMembers(project_id, UserType.admin));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setProjectLogs(allProjectLogs);
    }, [allProjectLogs]);

    const onAddMemberHandler = async (id) => {
        await dispatch(actions.addProjectMember(project_id, id))
        dispatch(actions.getProjectMembers(project_id, UserType.user));
    }

    const onDeleteMemberHandler = async (id) => {
        await dispatch(actions.deleteProjectMember(project_id, id))
        dispatch(actions.getProjectMembers(project_id, UserType.user));
    }

    return (
        <div className="main-body">
            <Modal
                isOpen={addMembersModalOpen}
                onRequestClose={closeAddMemberModal}
                style={ModalStyle}
                contentLabel="Example Modal"
            >
                <div className='d-flex justify-between pointer'>
                    <h2>Add Members</h2>
                    <div onClick={closeAddMemberModal}><Close/></div>
                </div>
                <p>Search Members to add</p>
                <div className='d-flex flex-column'>
                    <div className='mt-md user-list member-list'>
                        {
                            users && users.length > 0 ?
                                users.map((member) => (
                                    <MemberItem
                                        id={member.id}
                                        firstName={member.first_name}
                                        lastName={member.last_name}
                                        username={member.username}
                                        type={MemberCardType.ADD}
                                        closeAddMemberModal={closeAddMemberModal}
                                        addMember={onAddMemberHandler}
                                    />
                                )) :
                                <div>No members added yet.</div>
                        }
                    </div>
                </div>
            </Modal>
            <div className="admin-projects-details d-flex">
                <div className="flex-3">
                    <div className="page-title-section">
                        <div className="page-title">Projects</div>
                    </div>
                    <div className="task-lists">
                        {
                            projectLogs && projectLogs.length > 0 ?
                                projectLogs.map((project) => (
                                    <div className="task-item">
                                        <TaskCard
                                            username={project.user.username}
                                            taskDate={project.date}
                                            taskName={project.task}
                                            description={project.description}
                                        />
                                    </div>
                                )) :
                                <div>No work logs yet.</div>
                        }
                    </div>
                </div>
                <div className="members-section flex-1 d-flex flex-column">
                    <div className="team-members-section card flex-2">
                        <div className="member-list-header d-flex justify-between">
                            <div className="bold">Team Members</div>
                            <div className="pointer" onClick={openAddMembersModal}><Add/></div>
                        </div>
                        <div className="member-list">
                            {
                                userMembers && userMembers.length > 0 ?
                                    userMembers.map((member) => (
                                        <MemberItem
                                            id={member.user.id}
                                            firstName={member.user.first_name}
                                            lastName={member.user.last_name}
                                            username={member.user.username}
                                            deleteMember={onDeleteMemberHandler}
                                            type={MemberCardType.DELETE}
                                        />
                                    )) :
                                    <div>No members added yet.</div>
                            }
                        </div>
                    </div>
                    <div className="team-members-section card flex-1">
                        <div className="member-list-header d-flex justify-between">
                            <div className="bold">Admins</div>
                            <div className="pointer"><Add/></div>
                        </div>
                        <div className="member-list">
                            {
                                adminMembers && adminMembers.length > 0 ?
                                    adminMembers.map((member) => (
                                        <MemberItem
                                            id={member.user.id}
                                            firstName={member.user.first_name}
                                            lastName={member.user.last_name}
                                            username={member.user.username}
                                            type={MemberCardType.DELETE}
                                        />
                                    )) :
                                    <div>No members added yet.</div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProjectDetails;

const ModalStyle = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
        borderRadius: 16
    },
};
