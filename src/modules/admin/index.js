import React, {useState} from 'react';
import AdminRoute from "./routes/adminRoute";
import Navbar from './components/Navbar';
import Sidebar from './components/Sidebar';
import './dashboard.scss';
import * as action from "../../store/actions";
import {useDispatch} from "react-redux";

const AdminLayout = () => {
    const dispatch = useDispatch();

    const [selectedTab, setSelectedTab] = useState("Dashboard");
    const changeDisplay = (val) => {
        setSelectedTab(val)
    }

    const handleLogout = () => {
        dispatch(action.logoutUser());
        window.location.reload(true);
    };

    return (
        <>
            <div className="main-dash">
                <Sidebar selectedTab={selectedTab} changeDisplay={changeDisplay} handleLogout={handleLogout}/>

                <div className="dashboard-body">
                    <Navbar selectedTab={selectedTab}/>
                    <AdminRoute/>
                </div>
            </div>
        </>
    );

};

export default AdminLayout;
