import React from 'react';
import {Switch, Route} from 'react-router-dom'
import routeList from "./routeList";


const AdminRoute = () => {
    return (
        <Switch>
            {
                routeList.map((route, key) => (
                    <Route path={`/admin${route.path}`} name={route.name} component={route.component}
                           exact={route.exact} key={key}/>
                ))
            }
        </Switch>
    );
};

export default AdminRoute;
