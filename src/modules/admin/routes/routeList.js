import Dashboard from "../views/Dashboard.js";
import Profile from "../views/Profile";
import UserList from '../views/UserList';
import AdminUserList from '../views/AdminUserList';
import Notifications from '../views/Notifications';
import Messages from '../views/Messages';
import Performance from '../views/Performance';
import AddUser from '../views/AddUser';
import AddAdminUser from "../views/AddAdminUser.js";
import Projects from "../views/Projects.js";
import ProjectForm from "../views/ProjectForm.js";
import ProjectDetails from "../views/ProjectDetails.js";

export default [
    {
        path: "",
        name: "admin",
        component: Dashboard,
        exact: true,
    },
    {
        path: "/profile",
        name: "admin.profile",
        component: Profile,
        exact: true,
    },
    {
        path: "/users",
        name: "admin.users",
        component: UserList,
        exact: true,
    },
    {
        path: "/admin-users",
        name: "admin.admin-user",
        component: AdminUserList,
        exact: true,
    },
    {
        path: "/notifications",
        name: "admin.notifications",
        component: Notifications,
        exact: true,
    },
    {
        path: "/messages",
        name: "admin.messages",
        component: Messages,
        exact: true,
    },
    {
        path: "/performance",
        name: "admin.performance",
        component: Performance,
        exact: true,
    },
    {
        path: "/users/add",
        name: "admin.add-user",
        component: AddUser,
        exact: true,
    },
    {
        path: "/users/edit/:id",
        name: "admin.edit-user",
        component: AddUser,
        exact: true,
    },
    {
        path: "/admin-users/add",
        name: "admin.add-admin-user",
        component: AddAdminUser,
        exact: true,
    },
    {
        path: "/admin-users/edit/:id",
        name: "admin.edit-admin-user",
        component: AddAdminUser,
        exact: true,
    },
    {
        path: "/projects",
        name: "admin.projects",
        component: Projects,
        exact: true,
    },
    {
        path: "/projects/add",
        name: "admin.add-project",
        component: ProjectForm,
        exact: true,
    },
    {
        path: "/projects/:id",
        name: "admin.project-details",
        component: ProjectDetails,
        exact: true,
    },
]
