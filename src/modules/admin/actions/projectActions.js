import apolloClient from "../../../config/apolloclient";
import gql from "graphql-tag";
import * as actions from "../../../store/actions";
import * as actionType from "./adminActionType";
import {UserType} from "../../../enum/UserType";
import store from "../../../store/store";

const getProjectsSuccess = (logs) => {
    return {
        type: actionType.GET_PROJECTS,
        payload: logs
    }
}

const getProjectsByUserIdSuccess = (projects) => {
    return {
        type: actionType.GET_PROJECTS_BY_USER_ID,
        payload: projects
    }
}

const getProjectLogsSuccess = (logs) => {
    return {
        type: actionType.GET_PROJECT_LOGS,
        payload: logs
    }
}

const getProjectLogsByProjectIdAndUserIdSuccess = (logs) => {
    return {
        type: actionType.GET_PROJECT_LOGS_BY_PROJECT_ID_AND_USER_ID,
        payload: logs
    }
}

const getCalculatedProjectLogsSuccess = (logs) => {
    return {
        type: actionType.GET_CALCULATED_PROJECT_LOGS,
        payload: logs
    }
}

const getProjectUserMembersSuccess = (members) => {
    return {
        type: actionType.GET_PROJECT_USER_MEMBERS,
        payload: members
    }
}

const getProjectAdminMembersSuccess = (members) => {
    return {
        type: actionType.GET_PROJECT_ADMIN_MEMBERS,
        payload: members
    }
}

export const getProjects = () => async (dispatch) => {
    let result = await apolloClient.mutate({
        mutation: gql`
            query MyQuery {
                projects {
                    id
                    project_name
                    description
                }
            }
        `
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching project logs"
            )
        );
    });
    if (result.data.projects) {
        dispatch(getProjectsSuccess(result.data.projects));
    }
}

export const getProjectsByUserId = () => async (dispatch) => {
    const userId = store.getState().authenticationReducer.loginUser.id
    await apolloClient.mutate({
        mutation: gql`
            query MyQuery {
                projects(where: {user_projects: {user_id: {_eq: "${userId}"}}}) {
                    id
                    project_name
                    description
                }
            }
        `, update: (_cache, result) => {
            dispatch(getProjectsByUserIdSuccess(result.data.projects));
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching projects"
            )
        );
    });
}

export const getCalculatedProjectLogs = () => async (dispatch) => {
    await apolloClient.mutate({
        mutation: gql`
            query MyQuery {
                user_log {
                    id
                    project_id
                    start_time
                    end_time
                    user_id
                }
            }
        `, update: (_cache, result) => {
            dispatch(getCalculatedProjectLogsSuccess(result.data.user_log));
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching project logs"
            )
        );
    });
}

export const getProjectLogs = (project_id) => async (dispatch) => {
    await apolloClient.mutate({
        mutation: gql`
            query MyQuery (
                $project_id: bigint!
            ) {
                user_log(where: {
                    project_id: {_eq: $project_id}
                }) {
                    id
                    task
                    description
                    date
                    user {
                        id
                        username
                    }
                }
            }
        `, update: (_cache, result) => {
            dispatch(getProjectLogsSuccess(result.data.user_log));
        },
        variables: {
            project_id: project_id
        }
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching project logs"
            )
        );
    });
}

export const getProjectLogsByProjectIdAndUserId = (project_id) => async (dispatch) => {
    const userId = store.getState().authenticationReducer.loginUser.id
    await apolloClient.mutate({
        mutation: gql`
            query MyQuery (
                $project_id: bigint!,
                $user_id: Int!
            ) {
                user_log(where: {
                    project_id: {_eq: $project_id}, user_id: { _eq: $user_id}
                }) {
                    id
                    task
                    description
                    date
                    start_time
                    end_time
                    time_spent
                    user {
                        id
                        username
                    }
                }
            }
        `,
        variables: {
            project_id: project_id,
            user_id: userId
        }, update: (_cache, result) => {
            dispatch(getProjectLogsByProjectIdAndUserIdSuccess(result.data.user_log));
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching project logs"
            )
        );
    });
}

export const getProjectLogsByUserId = () => async (dispatch) => {
    const userId = store.getState().authenticationReducer.loginUser.id
    await apolloClient.mutate({
        mutation: gql`
            query MyQuery (
                $user_id: Int!
            ) {
                user_log(where: {
                    user_id: { _eq: $user_id}
                }) {
                    id
                    task
                    description
                    date
                    start_time
                    end_time
                    time_spent
                    user {
                        id
                        username
                    }
                }
            }
        `,
        variables: {
            user_id: userId
        }, update: (_cache, result) => {
            dispatch(getProjectLogsByProjectIdAndUserIdSuccess(result.data.user_log));
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching project logs"
            )
        );
    });
}

export const addProject = (project) => async (dispatch) => {
    await apolloClient.mutate({
        mutation: gql`
            mutation MyMutation (
                $project_name: String!,
                $created_at: timestamptz!,
                $description: String!
            ) {
                insert_projects(objects: {
                    project_name: $project_name,
                    description: $description,
                    created_at: $created_at
                }) {
                    returning {
                        id
                    }
                }
            }
        `,
        variables: {
            project_name: project.project_name,
            description: project.description,
            created_at: project.created_at
        },
        update: (_cache, result) => {
            dispatch(
                actions.setToasterState(
                    true,
                    "Success",
                    "Project Added ",
                    "Project is created"
                )
            );
            return "success"
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while adding the project"
            )
        );
    });
}

export const getProjectMembers = (project_id, user_type) => async (dispatch) => {
    await apolloClient.mutate({
        mutation: gql`
            query MyQuery (
                $project_id: bigint!, $user_type: user_type_enum!
            ) {
                user_project(where: {
                    project_id: {_eq: $project_id}, user: {type: {_eq: $user_type}}
                }) {
                    id
                    project_id
                    user {
                        id
                        first_name
                        last_name
                        username
                    }
                }
            }
        `,
        variables: {
            project_id: project_id,
            user_type: user_type
        },
        update: (_cache, result) => {
            if (user_type === UserType.user)
                dispatch(getProjectUserMembersSuccess(result.data.user_project));
            else
                dispatch(getProjectAdminMembersSuccess(result.data.user_project));
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching project members"
            )
        );
    });
}

export const addProjectMember = (projectId, userId) => async (dispatch) => {
    await apolloClient.mutate({
        mutation: gql`
            mutation MyQuery (
                $project_id: bigint!, $user_id: bigint
            ) {
                insert_user_project(objects: {project_id: $project_id, user_id: $user_id}) {
                    returning {
                        id
                    }
                    affected_rows
                }
            }
        `,
        variables: {
            project_id: projectId,
            user_id: userId
        },
        update: (_cache, result) => {
            dispatch(
                actions.setToasterState(
                    true,
                    "Success",
                    "Member has been added"
                )
            );
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while adding project members"
            )
        );
    });
}

export const deleteProjectMember = (projectId, userId) => async (dispatch) => {
    await apolloClient.mutate({
        mutation: gql`
            mutation MyQuery (
                $project_id: bigint!, $user_id: bigint
            ) {
                delete_user_project(where: {project_id: {_eq: $project_id}, user_id: {_eq: $user_id}}) {
                    affected_rows
                }
            }
        `,
        variables: {
            project_id: projectId,
            user_id: userId
        },
        update: (_cache, result) => {
            dispatch(
                actions.setToasterState(
                    true,
                    "Success",
                    "Member has been removed"
                )
            );
        },
    }).catch((err) => {
        console.log(err)
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while removing project members"
            )
        );
    });
}
