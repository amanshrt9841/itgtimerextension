import apolloClient from "../../../config/apolloclient";
import gql from "graphql-tag";
import * as actions from "../../../store/actions";
import {UserType} from "../../../enum/UserType";
import * as actionType from "./adminActionType";


const getAdminUsersSuccess = (users) => {
    return {
        type: actionType.GET_ADMIN_USERS,
        payload: users
    }
}

const getUsersSuccess = (users) => {
    return {
        type: actionType.GET_USERS,
        payload: users
    }
}

const getUserByIdSuccess = (userById) => {
    return {
        type: actionType.GET_USER_BY_ID,
        userById
    }
}

export const getAdminUsers = () => async (dispatch) => {
    await apolloClient.mutate({
        mutation: gql`
            query MyQuery {
                users(where: {type: {_eq: ${UserType.admin}}}) {
                    id
                    first_name
                    last_name
                    email
                    type
                    username
                }
            }
        `, update: (_cache, result) => {
            dispatch(getAdminUsersSuccess(result.data.users));
        },
    }).catch(error => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching admins"
            )
        );
    })
}

export const getUsers = () => async (dispatch) => {
    let result = await apolloClient.mutate({
        mutation: gql`
            query MyQuery {
                users(where: {type: {_eq: ${UserType.user}}}) {
                    id
                    first_name
                    last_name
                    email
                    type
                    username
                }
            }
        `
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching users"
            )
        );
    });

    if (result.data.users) {
        dispatch(getUsersSuccess(result.data.users));
    }
}

export const addUser = (user) => async (dispatch) => {
    let result = await apolloClient.mutate({
        mutation: gql`
            mutation MyMutation(
                $email: String!,
                $password: String!,
                $first_name: String!,
                $last_name: String!,
                $username: String!,
                $type: user_type_enum!
            ) {
                insert_users(objects: {
                    email: $email,
                    password: $password,
                    type: $type,
                    first_name: $first_name,
                    last_name: $last_name,
                    username: $username
                }) {
                    affected_rows
                    returning {
                        email
                        id
                        username
                    }
                }
            }
        `,
        variables: {
            email: user.email,
            password: user.password,
            first_name: user.first_name,
            last_name: user.last_name,
            username: user.username,
            type: user.type
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem creating user ",
                "User wasn't created"
            )
        );
    });

    if (result) {
        dispatch(
            actions.setToasterState(
                true,
                "Success",
                "User Added ",
                "User is created"
            )
        );
        return "success";
    }
}

export const updateUser = (user, userId) => async (dispatch) => {
    let result = await apolloClient.mutate({
        mutation: gql`
            mutation MyMutation(
                $userId: Int!,
                $email: String!,
                $password: String!,
                $first_name: String!,
                $last_name: String!,
                $username: String!,
            ) {
                update_users(where: {id: {_eq: $userId}}, _set: {email: $email, first_name: $first_name, last_name: $last_name, password: $password, username: $username}) {
                    affected_rows
                }
            }
        `,
        variables: {
            userId: userId,
            email: user.email,
            password: user.password,
            first_name: user.first_name,
            last_name: user.last_name,
            username: user.username,
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem updating user ",
                "User wasn't updated"
            )
        );
    });

    if (result) {
        dispatch(
            actions.setToasterState(
                true,
                "Success",
                "User Updated ",
                "User updated successfully"
            )
        );
        return "success";
    }
}

export const getUserById = (userId) => async (dispatch) => {
    apolloClient.mutate({
        mutation: gql`
            query MyQuery($userId: Int!) {
                users(where: {id: {_eq: $userId}}) {
                    email
                    first_name
                    id
                    last_name
                    username
                }
            }

        `,
        variables: {
            userId: userId
        },
        update: (_cache, result) => {
            console.log(result.data)
            dispatch(getUserByIdSuccess(result.data.users[0]))
        },
    }).catch(error => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem Fetching User Data",
                "Can't Fetch User Data"
            )
        );
    })
}
