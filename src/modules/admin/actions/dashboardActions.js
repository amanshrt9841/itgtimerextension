// import * as actionType from './logType';
import apolloClient from "../../../config/apolloclient";
import gql from "graphql-tag";
import * as actions from "../../../store/actions";
import {UserType} from "../../../enum/UserType";

export const getUsers = () => async (dispatch) => {
    let x = await apolloClient.mutate({
        mutation: gql`
            query MyQuery {
                users(where: { type: { _eq: ${UserType.user} } }) {
                    email
                    id
                }
            }
        `
    });
    return x;
};

export const getWorksAccomplished = (id) => async (dispatch) => {
    try {
        let x = await apolloClient.mutate({
            mutation: gql`
                query MyQuery($userid : Int! ) {
                    user_log(where: {user_id: {_eq: $userid}}) {
                        id
                        detail
                        title
                        total_time
                    }
                }
            `,
            variables: {
                userid: id,
            },
        });
        return x;
    } catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem creating user ",
                "User wasn't created"
            )
        );
    }

}

// export const getHourstrackedPerworker = (id) => async (dispatch) => {
//     try {
//         let x = await apolloClient.mutate({
//             mutation: gql`
//                 query MyQuery($userid : Int! ) {
//                     time_tracker(where: {user_id: {_eq: $userid}}) {
//                         time_tracked
//                         created_at
//                     }
//                 }
//             `,
//             variables: {
//                 userid: id,
//             },
//         });
//         return x;
//     } catch (e) {
//         dispatch(
//             actions.setToasterState(
//                 true,
//                 "Error",
//                 "Problem creating user ",
//                 "User wasn't created"
//             )
//         );
//     }
// }

// export const getUsersHours = () => async (dispatch) => {
//     let x = await apolloClient.mutate({
//         mutation: gql`
//             query MyQuery {
//                 time_tracker {
//                     time_tracked
//                 }
//             }
//         `,
//     });
//     return x;
// };
