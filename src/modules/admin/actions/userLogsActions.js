import store from "../../../store/store";
import apolloClient from "../../../config/apolloclient";
import gql from "graphql-tag";
import * as actions from "../../../store/actions";
import * as actionType from "./adminActionType";

const getUserLogsSuccess = (logs) => {
    return {
        type: actionType.GET_USER_LOGS,
        payload: logs
    }
}

const getUserLogsByUserIdSuccess = (logs) => {
    return {
        type: actionType.GET_USER_LOGS_BY_USER_ID,
        payload: logs
    }
}

export const getUserLogs = () => async (dispatch) => {
    let result = await apolloClient.mutate({
        mutation: gql`
            query MyQuery{
                user_log {
                    id
                    task
                    description
                    date
                    start_time
                    end_time
                    user {
                        id
                        username
                    }
                }
            }
        `,
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching logs"
            )
        );
    });
    if (result.data.user_log) {
        dispatch(getUserLogsSuccess(result.data.user_log));
    }
}

export const getUserLogsByUserId = () => async (dispatch) => {
    const userId = store.getState().authenticationReducer.loginUser.id
    await apolloClient.mutate({
        mutation: gql`
           query MyQuery(
                $user_id: Int!
           ){
                user_log(where: {user_id: { _eq: $user_id}, time_spent: {_is_null: false}},order_by: {end_time: desc}) {
                    id
                    task
                    description
                    date
                    start_time
                    end_time
                    time_spent
                    user {
                        id
                        username
                    }
                }
           }
        `,
        variables: {
            user_id: userId
        },
        update: (_cache, result) => {
            dispatch(getUserLogsByUserIdSuccess(result.data.user_log));
        },
    }).catch((err) => {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Problem while fetching logs"
            )
        );
    });
}
