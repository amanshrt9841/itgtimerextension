import React from 'react';
import {useSelector} from "react-redux";
import {Route, Redirect} from 'react-router-dom'
import {UserType} from "../enum/UserType";

const PublicRoute = ({component: Component, setToasterState, ...rest}) => {

    const {isLoggedIn} = useSelector(state => state.authenticationReducer)
    const {userType} = useSelector(state => state.authenticationReducer)

    return (
        <Route {...rest} render={
            props => isLoggedIn ? (
                    userType === UserType.user ? <Redirect to="/client/add-logs"/> : <Redirect to='/admin'/>
                ) :
                (<Component {...props} />)
        }
        />
    );
};

export default PublicRoute
