import React from 'react';
import {Switch} from 'react-router-dom'
import routeList from "./routeList";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

const MainRoutes = () => {

    return (
        <Switch>
            {routeList.map((route, key) =>
                !route.isAuth ? (
                    <PublicRoute
                        path={`${route.path}`}
                        name={route.name}
                        component={route.component}
                        exact={route.exact}
                        key={key}
                    />
                ) : (
                    <PrivateRoute
                        path={`${route.path}`}
                        name={route.name}
                        component={route.component}
                        exact={route.exact}
                        key={key}
                    />
                )
            )}
        </Switch>
    );
};

export default MainRoutes;
