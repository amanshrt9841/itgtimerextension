import AdminLayout from '../modules/admin';
import Client from "../modules/client";
import Login from '../modules/Authentication/container/login';
import LogForm from '../modules/landing/component/LogForm';

export default [
  {
    path: "/",
    name: "clientLogin",
    component: Login,
    isAuth: false,
    exact: true,
  },
  {
    path: "/admin/login",
    name: "adminLogin",
    component: Login,
    isAuth: false,
    exact: true,
  },
  {
    path: "/admin",
    name: "admin",
    component: AdminLayout,
    isAuth: true,
    exact: false,
  },
  {
    path: "/client",
    name: "client",
    component: Client,
    isAuth: true,
    exact: false,
  },
  {
    path: "/logform",
    name: "LogForm",
    component:  LogForm  ,
    isAuth: true,
  },
];
