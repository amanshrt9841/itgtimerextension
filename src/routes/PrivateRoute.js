import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Redirect, Route} from 'react-router-dom'
import * as actions from "../common/Toaster/services/ToasterActions";

const PrivateRoute = (props) => {
        const {component: Component, ...rest} = props;
        const dispatch = useDispatch()

        const {isLoggedIn} = useSelector(state => state.authenticationReducer)

        const openToaster = () => {
            dispatch(actions.setToasterState(
                true,
                "error",
                "Authentication Error",
                "You have to authenticate before proceedings "
            ))
        }

        useEffect(() => {
            !isLoggedIn && openToaster()
            console.log("Logged in",isLoggedIn)
        }, [isLoggedIn])

        return (
            <Route {...rest} render={
                props => !isLoggedIn ? (<Redirect to="/"/>) :
                    (<Component {...props} />)
            }
            />
        );
    }
;

export default PrivateRoute
