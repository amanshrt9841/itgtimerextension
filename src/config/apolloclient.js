import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

//cache variable that hold cache values.
const cache = new InMemoryCache();

const getHeaders = () => {
  const headers = {
    "content-type": "application/json",
    "x-hasura-admin-secret":
      "jCFCWMDfNKVeySHZh5f8QsJC4Cg66cqs2NcMn0uKDEi66nB9sduQCmvjvmZbIE56",
  };
  return headers;
};

const link = new HttpLink({
  //graphql api uri
  uri: process.env.REACT_APP_DB_URL,
  fetch,
  headers: getHeaders(),
});

const apolloClient = new ApolloClient({
  cache,
  link,
});

export default apolloClient;
