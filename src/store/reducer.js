import { combineReducers } from 'redux'
import modalReducer from "../common/Modal/services/modalReducer";
import toasterReducer from "../common/Toaster/services/ToasterReducer";
import loadingReducer from "../common/Loading/services/loadingReducer";
import authenticationReducer from "../modules/Authentication/services/authenticationReducer";
import timerReducer from '../modules/client/component/TimerCard/services/timerReducer'
import logReducer from "../modules/client/services/logReducer";
import adminReducer from "../modules/admin/reducers/adminReducer";

export default combineReducers({
    modalReducer,
    toasterReducer,
    loadingReducer,
    authenticationReducer,
    timerReducer,
    logReducer,
    adminReducer
})
