import React, {useEffect} from 'react';
import './App.scss';
import MainRoutes from "./routes";
import * as actions from "./store/actions";
import {useDispatch} from "react-redux";
import Toaster from "./common/Toaster/Toaster";

const App = () => {

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(actions.checkAuthentication())

    }, [])

    return (
        <>
            <MainRoutes/>
            <Toaster/>
        </>
    );
}

export default App;
