import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {Provider} from "react-redux";
import {HashRouter} from "react-router-dom";
import { createMemoryHistory } from 'history'
import { ApolloProvider } from "@apollo/react-hooks";

import store from "./store/store";
import apolloClient from "./config/apolloclient";

const history = createMemoryHistory()

const app = (
    <ApolloProvider client={apolloClient}>
        <Provider store={store}>
            <HashRouter history={history}>
                <App/>
            </HashRouter>
        </Provider>
    </ApolloProvider>
)

ReactDOM.render(
    app,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
